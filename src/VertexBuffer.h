#pragma once
#include <GL/glew.h>
class VertexBuffer
{
private:
	GLuint RendererID;
public:
	VertexBuffer(const void* Vdata, GLuint size);
	~VertexBuffer();

	void Attach();
	void Dettach();
};