#pragma once
#include <string>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

struct ShaderProgramSource
{
	std::string vertexSource;
	std::string fragmentSource;
};

class Shader 
{
public:
	GLuint RendererID;
	std::string filepath;

	GLuint GetUniformLocation(const std::string& name);
	GLint CompileShader(const std::string& source, unsigned int type);
	GLint CreateShader(const std::string& vertexShader, const std::string& fragmentShader);
	ShaderProgramSource ParseShader(const std::string& filepath);

public:
	Shader(const std::string fpath);
	~Shader();

	void Attach() const;
	void Detach() const;
	void SetUniformMatrix4fv(const std::string& name, GLuint n, bool to_norm, glm::mat4 proj);
	void SetUniform4f(const std::string& name, float s);

};