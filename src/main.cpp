#include "renderer.h"
#include "VertexBuffer.h"
#include "IndexBuffer.h"
#include "Shader.h"
#include <GLFW/glfw3.h>


void mouse_callback(GLFWwindow* window, int button, int action, int mods);
void scroll_callback(GLFWwindow* window, double xoffset, double yoffset);
void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods);


int main(void)
{
    //Window object
    GLFWwindow* window;

    /* Initialize the library */
    if (!glfwInit())
        return -1;

    // Setting window hints, setting up the OpenGL context
    glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, true);
    glfwWindowHint(GLFW_RESIZABLE, GLFW_FALSE);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);


    /* Create a windowed mode window and its OpenGL context */
    window = glfwCreateWindow(width, height, "Hello 3D World!", NULL, NULL);
    if (!window)
    {
        glfwTerminate();
        return -1;

    }

    /* Make the window's context current */
    glfwMakeContextCurrent(window);
    /*Linking the mouse event call back functions for
    Left button click and scrolling actions/events
    */
    glfwSetMouseButtonCallback(window, mouse_callback);
    glfwSetKeyCallback(window, key_callback);
    glfwSetScrollCallback(window, scroll_callback);

    //Error handling
    if (glewInit() != GLEW_OK)
        std::cout << "Error!" << std::endl;
    std::cout << glGetString(GL_VERSION) << std::endl;

    // Eanable capture of debug output.
    glEnable(GL_DEBUG_OUTPUT);
    glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
    glDebugMessageCallback(MessageCallback, 0);
    glDebugMessageControl(GL_DONT_CARE, GL_DONT_CARE, GL_DONT_CARE, 0, nullptr, GL_TRUE);
    
    //for 3D, enable depth testing with z-buffer to avoid over-drawing of pixels
    glEnable(GL_DEPTH_TEST);
    //cube vertices
    GLfloat cube[48] =
    {
     //position            color
    -0.5f, -0.5f, -0.5f,0.0f, 1.0f, 0.0f,
    0.5f, -0.5f,-0.5f, 0.0f,1.0f, 0.0f,
    0.5f, 0.5f,-0.5f,0.0f,1.0f, 0.0f,
    -0.5f, 0.5f,-0.5f, 0.0f,1.0f, 0.0f,
    -0.5f, -0.5f, 0.5f,1.0f, 0.0f, 0.0f,
    0.5f, -0.5f,0.5f, 1.0f,0.0f, 0.0f,
    0.5f, 0.5f, 0.5f,1.0f,0.0f,0.0f,
    -0.5f, 0.5f,0.5f, 1.0f,0.0f,0.0f,
    };
        
    GLuint cube_indices[6*6] = 
    { 0, 1, 3, 3, 1, 2,
    1, 5, 2, 2, 5, 6,
    5, 4, 6, 6, 4, 7,
    4, 0, 7, 7, 0, 3,
    3, 2, 7, 7, 2, 6,
    4, 5, 0, 0, 5, 1 };
    
    /* world space positions of cubes
    we wanted to duplicate our cube to.
    */
    glm::vec3 cubePositions[] = 
    {
        glm::vec3(0.0f,  0.0f,  0.0f),
        glm::vec3(2.0f,  5.0f, 15.0f),
        glm::vec3(2.0f,  5.0f, -15.0f),
        glm::vec3(-1.5f, -2.2f, -2.5f)
    };
    
    /*Generate your vertex array object and 
    link all the required buffers and shaders 
    */
    ////////////////////////////
    GLuint vao;
    glCreateVertexArrays(1, &vao);
    glBindVertexArray(vao);
        
    VertexBuffer vb(cube, sizeof(cube));
    IndexBuffer ib(cube_indices,6*6);
    vb.Attach();
    ib.Attach();
    
    //auto RVAO = DrawRectangle();
    std::string spath = "res/shaders/camera.shader";
    Shader shader(spath);
    
    // Specify the layout of the vertex data
    GLint posAttrib = glGetAttribLocation(shader.RendererID, "position");
    glEnableVertexAttribArray(posAttrib);
    glVertexAttribPointer(posAttrib, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(GLfloat), 0);

    GLint colAttrib = glGetAttribLocation(shader.RendererID, "color");
    glEnableVertexAttribArray(colAttrib);
    glVertexAttribPointer(colAttrib, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(GLfloat), (void*)(3 * sizeof(GLfloat)));
    ////////////////////////////////

    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    
    float currentTime = 0.0f;
    glfwSetTime(0.0);
    /* Loop until the user closes the window */
    while (!glfwWindowShouldClose(window))
    {
        /* Poll for and process events */
        glfwPollEvents();

        /* 
        Code adopted from learnopengl.com
        */
        // per-frame time logic
        // --------------------
        float currentFrame = glfwGetTime();
        deltaTime = currentFrame - lastFrame;
        lastFrame = currentFrame;

        currentTime = currentFrame;
        /* Render here */
        glClear(GL_COLOR_BUFFER_BIT);
        // also clear the depth buffer. 
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        shader.Attach();

        // Setingup transformation
        ////////////////////////////////////
        // pass projection matrix to shader 
                                                      //fov
        glm::mat4 projection = glm::perspective(glm::radians(camera.Zoom), 
                                    (float)width / (float)height, 0.1f, 100.0f);

        // camera/view transformation
        glm::mat4 view = camera.GetViewMatrix();
        // pass transformation matrices to the shader
        shader.SetUniformMatrix4fv("projection", 1, GL_FALSE, projection);
        shader.SetUniformMatrix4fv("view", 1, GL_FALSE, view);

        glBindVertexArray(vao);
        shader.SetUniform4f("scaleColor", 1.0f);
        
        // render cubes
        glBindVertexArray(vao);
        for (unsigned int i = 0; i < 4; i++)
        {
            // calculate the model matrix for each object and pass it to shader before drawing
            glm::mat4 model = glm::mat4(1.0f);
            model = glm::translate(model, cubePositions[i]);
            float angle = 10.0f * float(currentTime);
            model = glm::rotate(model, glm::radians(angle), glm::vec3(1.0f, 0.3f, 0.5f));
            shader.SetUniformMatrix4fv("model", 1, GL_FALSE, model);

            glDrawElements(GL_TRIANGLES,36, GL_UNSIGNED_INT, 0);
        }

        ////////////////////////////////////

        /* Swap front and back buffers */
        glfwSwapBuffers(window);

        if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
        {
            break;
        }

    }

    glfwTerminate();
    return 0;
}

// glfw: whenever the mouse moves, this callback is called
// -------------------------------------------------------
void mouse_callback(GLFWwindow* window, int button, int action, int mods)
{
    if (button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_PRESS)
    {
        double xpos, ypos;
        glfwGetCursorPos(window, &xpos, &ypos);
        if (firstMouse)
        {
            lastX = xpos;
            lastY = ypos;
            firstMouse = false;
        }

        float xoffset = xpos - lastX;
        float yoffset = lastY - ypos; // reversed since y-coordinates go from bottom to top

        lastX = xpos;
        lastY = ypos;

        camera.ProcessMouseMovement(xoffset, yoffset);
    }
    
}

void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
    if (key == GLFW_KEY_UP && action == GLFW_PRESS)
    {
        camera.ProcessKeyboard(FORWARD, 0.5f);
        std::cout << "UP";
    }
    if (key == GLFW_KEY_DOWN && action == GLFW_PRESS)
    {
        camera.ProcessKeyboard(BACKWARD, 0.5f);
    }
    if (key == GLFW_KEY_RIGHT && action == GLFW_PRESS)
    {
        camera.ProcessKeyboard(RIGHT, 0.5f);
    }
    if (key == GLFW_KEY_LEFT && action == GLFW_PRESS)
    {
        camera.ProcessKeyboard(LEFT, 0.5f);
    }
        
}


// glfw: whenever the mouse scroll wheel scrolls, this callback is called
// ----------------------------------------------------------------------
void scroll_callback(GLFWwindow* window, double xoffset, double yoffset)
{
    camera.ProcessMouseScroll(yoffset);
}